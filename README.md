# Camagru

Camagru is an Instagram imitation website allowing users to create profiles and post edited photos to a public gallery.

## Requirements:
 - PHP 7+
 - WAMP (or any other Apache server)
 - MySQL 5+
 - Apache Server 2
All requirements come installed standard with Bitnami WAMP (https://bitnami.com/stack/wamp)

## Installation
 ### Source code
  - Source code is available for download from https://gitlab.com/Androva/camagru. Select clone or download the available zipped file.  
 
 ### Setup
  - Install Bitnami WAMP and follow the installation wizard which will install PHP, Apache 2 as well as MySQL.
  - When selecting modules to download for WAMP, the only necessary requirement for this project is phpMyAdmin.
  - Ensure that you make note of the password you specify for MySQL as this is required to log into phpMyAdmin.
  - After installation, ensure that the Apache server and MySQL database are running by opening the WAMP manager. If they are not, click restart (or start).
  - Select 'Configure' next to the MySQL instance to obtain the operating port. Do the same for Apache and note these port numbers.
  - Copy the source code files into `Bitnami\wampstack-7.4.10-0\apache2\htdocs`.
  - In a browser window, navigate to `http://localhost:[apache-port]/camagru/config/setup.php`. This will create the Camagru database and create some dummy users.
  - Navigate to `http://localhost/phpmyadmin`, log in and verify that the camagru database and it's related tables and users have been created.
  
## Running Camagru
 - Ensure that your Apache server is running and then you can navigate to `http://localhost:[apache-port]/camagru`.
 
## Backend
 - PHP 7+ (Server-side language and processing)
 - Apache 2 (Server)
 - MySQL 5+ (Database management tool)

## Frontend
 - HTML5 (markup language)
 - Javascript (scripting language)
 - CSS (styling language)
 
## Directory Structure
 - camagru
    - backend
        - change_credentials.php
            - User credentials (username, password, email) editing manager
        - comment.php
            - User commenting manager
        - delete.php
            - Image deleting manager
        - img_upload.php
            - Image database upload manager
        - index_credentials.php
            - User login, registration and password reset manager
        - like.php
            - Image liking manager
        - logout.php
            - User session logout manager
        - overlay.php
            - Image overlay and filter manager
        - reset_email.php
            - User email reset sending manager
        - save_image.php
            - User image database saving manager
            
    - config
        - connect.php
            - Database connection manager
        - database.php
            - Database credentials
        - setup.php
            - Database initialisation and setup
            
    - extras
        - overlays
            - Directory containing image overlays and filters
        - capture.js
            - Webcam image capturing and overlay placing script
        - comment.js
            - Comment adding script
        - like_comm.js
            - Like & comment adding script
        - style.css
            - Styling for the application
        - upload.js
            - Image uploading script
            
    - frontend
        - account.php
            - Account management page
        - comments.php
            - Comments viewing page
        - gallery.php
            - Public gallery viewing page
        - likes.php
            - Likes viewing page
        - upload.php
            - Image uploading page
        - webcam.php
            - Webcam image capturing page
            
    - index.php
        - Home page
            
    - verify.html
        - User registration verification page

## Testing
  https://github.com/wethinkcode-students/corrections_42_curriculum/blob/master/camagru.markingsheet.pdf